# -*- coding: utf-8 -*-

import factory
from models import Book, Author, Abonement

class AuthorFactory(factory.Factory):
	
	FACTORY_FOR = Author	

	name = factory.Sequence(lambda n: 'author {0}'.format(n))


class BookFactory(factory.Factory):

	FACTORY_FOR = Book

	author = factory.SubFactory(AuthorFactory)
	title = factory.Sequence(lambda n: 'book {0}'.format(n))


class AbonementFactory(factory.Factory):

	FACTORY_FOR = Abonement

	name = factory.Sequence(lambda n: 'abonement {0}'.format(n))