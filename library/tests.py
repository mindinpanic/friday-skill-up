# -*- coding: utf-8 -*-

import __builtin__

from random import choice
from django.test import TestCase
from mock import patch, Mock, MagicMock
import library.utils
from datetime import datetime

from factories import BookFactory, AuthorFactory, AbonementFactory

from tasks import add

def get_mock_file():
    mock_file = Mock()
    mock_file.name = 'fake.dat'
    mock_file.__iter__ =  lambda k: iter(xrange(10))
    stat = mock_file.stat.return_value
    stat.size, stat.access_time = 500, datetime(2012, 12, 29)
    return mock_file

class MockTest(TestCase):

    def test_patch(self):

        with patch('library.utils.big_range') as perm_mock:
            perm_mock.return_value = xrange(3)
            library.utils.long_cycle()
        # library.utils.long_cycle()


    def test_object_spy(self):
        mobj = Mock()

        library.utils.object_manipulator(mobj)

        attrs = ['called', 'call_count', 'call_args', 'call_args_list', 'method_calls', 'mock_calls']

        for attr in attrs:
            print attr, " => ", getattr(mobj, attr)


    def test_mock_file(self):
        f = get_mock_file()
        for l in f:
            print l

    def test_builtin(self):
        mock = MagicMock(return_value = ["qwe", "qwe"])
        with patch('__builtin__.open', mock):
            for l in open('filename', 'r'):
                print


class FactoryboyTest(TestCase):

    def setUp(self):
        self.books = [BookFactory() for _ in range(10)]
        self.abonements = [AbonementFactory() for _ in range(10)]

    def test_no_db(self):
        self.assertEqual(len(self.books), 10)
        self.assertEqual(len(self.abonements), 10)


    def test_book_limit(self):
        abonement = AbonementFactory()

        for i in range(10):
            abonement.books.add(self.books[i])

        self.assertEqual(abonement.check_if_book_limit_exceed(), True)


class CeleryTest(TestCase):
    
    def test_add(self):
        result = add.delay(4, 5)
        self.assertEqual(result.get(), 9)
        self.assertTrue(result.successful())