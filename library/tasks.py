# -*- coding: utf-8 -*-

from celery import task
import time
import random

@task()
def add(x, y):
	delay = random.randint(1,60)
	time.sleep(delay)
	return x + y