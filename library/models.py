# -*- coding: utf-8 -*-

from django.db import models

BOOK_PER_ABONEMENT_LIMIT = 5

class Author(models.Model):
	name = models.CharField(max_length=125)

	def __unicode__(self):
		return self.name

class Book(models.Model):
	title = models.CharField(max_length=125)
	author = models.ForeignKey(Author)

	def __unicode__(self):
		return self.title

class Abonement(models.Model):
	name = models.CharField(max_length=125)
	books = models.ManyToManyField(Book)

	def __unicode__(self):
		return self.name

	def check_if_book_limit_exceed(self):
		return self.books.all().count() > BOOK_PER_ABONEMENT_LIMIT