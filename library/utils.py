# -*- coding: utf-8 -*-

def big_range():
    return xrange(1000000)

def long_cycle():
    y = 0
    for i in big_range():
        y += i
        print y


def object_manipulator(obj):
    obj()
    obj(1,2,3,4)
    obj.run()
    obj.update({'foo': 'bar'}, is_dummy=True)
